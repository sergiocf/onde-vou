﻿delete from review;

delete from placeimage;

delete from place;

delete from category;

insert into category (id, name) values (1, 'Restaurante');
insert into category (id, name) values (2, 'Cinema');
insert into category (id, name) values (3, 'Boate');
insert into category (id, name) values (4, 'Parque'); 

insert into place (id, description, imageurl, name, rating, category_id, location) values (1, 'Comida simples a quilo com churrasco.', 'app/img/places/papo-legal.0.jpg', 
'Papo legal 3', 3, 1, ST_GeometryFromText('SRID=4326;POINT(-19.9571453 -43.9381027)'));

insert into place (id, description, imageurl, name, rating, category_id, location) values (2, 
'Estar no Chalé da Mata é contar com a integração de amigos e familiares em uma estrutura ampla.', 
'app/img/places/chale-da-mata.0.png', 'Chale da Mata', 4, 1, ST_GeometryFromText('SRID=4326;POINT(-19.9716181 -43.9618483)'));


insert into place (id, description, imageurl, name, rating, category_id, location) values (3, 
'No Verdemar você compra mais sabor em todos os sentidos.', 
'app/img/places/verde-mar.0.png', 'Verdemar', 3, 1, ST_GeometryFromText('SRID=4326;POINT(-19.954438 -43.939801)'));


insert into place (id, description, imageurl, name, rating, category_id, location) values (4, 
'Encravado na Serra do Curral, o Parque Municipal das Mangabeiras é um dos maiores e mais belos redutos ecológicos de Belo Horizonte.', 
'app/img/places/mangabeiras.0.jpg', 'Parque Municipal das Mangabeiras', 3, 4, ST_GeometryFromText('SRID=4326;POINT(-19.955393 -43.907827)'));


insert into place (id, description, imageurl, name, rating, category_id, location) values (5, 
'Os melhores cortes de carne do mundo estão aqui. ', 
'app/img/places/fogo-de-chao.0.jpg', 'Fogo de Chão Churrascaria', 5, 1, ST_GeometryFromText('SRID=4326;POINT(-19.936635 -43.938264)'));


insert into place (id, description, imageurl, name, rating, category_id, location) values (6, 
'Seja bem-vindo! Nascemos em Minas, terra considerada como um dos berços da culinária brasileira e onde a cozinha é o melhor lugar de uma casa.', 
'app/img/places/a-granel.0.jpg', 'A Granel', 3, 1, ST_GeometryFromText('SRID=4326;POINT(-19.925545 -43.990119)'));

insert into place (id, description, imageurl, name, rating, category_id, location) values (7, 
'Aproveite a Sessão Desconto Cinemark. Por um preço especial, você assiste a um filme em cartaz de 2ª a 6ªf, às 14h (exceto feriados)', 
'app/img/places/cinemark.0.jpg', 'Cinemark', 3, 2, ST_GeometryFromText('SRID=4326;POINT(-19.974980 -43.944971)'));


insert into placeimage values (1,'app/img/places/papo-legal.0.jpg',1);
insert into placeimage values (2,'app/img/places/papo-legal.1.jpg',1); 
insert into placeimage values (3,'app/img/places/papo-legal.2.jpg',1); 
insert into placeimage values (4,'app/img/places/papo-legal.3.jpg',1); 
insert into placeimage values (5,'app/img/places/papo-legal.4.jpg',1);

insert into review (id, author, description, stars, place_id) values (1, 'sergiocf@gmail.com', 'Muito simples mas comida gostosa!', 3, 1);
insert into review (id, author, description, stars, place_id) values (2, 'paulamsd@gmail.com', 'Não tem boas opções', 2, 1);

insert into placeimage values (6,'app/img/places/chale-da-mata.0.png',2);
insert into placeimage values (7,'app/img/places/chale-da-mata.1.jpg',2); 
insert into placeimage values (8,'app/img/places/chale-da-mata.2.jpg',2); 
                                                 
insert into placeimage values (9,'app/img/places/verde-mar.0.png',3);
insert into placeimage values (10,'app/img/places/verde-mar.1.jpg',3); 
insert into placeimage values (11,'app/img/places/verde-mar.2.jpg',3);

insert into placeimage values (12,'app/img/places/mangabeiras.0.jpg',4);
insert into placeimage values (13,'app/img/places/mangabeiras.1.jpg',4); 
insert into placeimage values (14,'app/img/places/mangabeiras.2.jpg',4);
insert into placeimage values (15,'app/img/places/mangabeiras.3.jpg',4);

insert into placeimage values (16,'app/img/places/fogo-de-chao.0.jpg',5);
insert into placeimage values (17,'app/img/places/fogo-de-chao.1.jpg',5); 
insert into placeimage values (18,'app/img/places/fogo-de-chao.2.jpg',5);

insert into placeimage values (19,'app/img/places/a-granel.0.jpg',6);
insert into placeimage values (20,'app/img/places/a-granel.1.jpg',6); 
insert into placeimage values (21,'app/img/places/a-granel.2.jpg',6);

insert into placeimage values (22,'app/img/places/cinemark.0.jpg',7);
insert into placeimage values (23,'app/img/places/cinemark.1.jpg',7);

commit;



