package com.ondevou.integration.test.rest;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import com.ondevou.entity.Place;
import com.ondevou.entity.PlaceImage;
import com.ondevou.entity.Review;

public class ITTestPlaceRestController {
	private String endpoint = "http://localhost:8080/onde-vou";

	@Test
	public void testGetPlace() {
		Place place = new Place();
		place.setName("Restaurant Name");
		place.setDescription("Restaurant description");

		PlaceImage image = new PlaceImage();
		image.setPath("/test/image.jpg");
		place.addImage(image);
		
		Review review = new Review();
		review.setAuthor("test@gmail.com");
		review.setDescription("Very good!");
		review.setStars(5);
		place.addReview(review);

		savePlace(place);

		place = getPlace(1);

		System.out.println("Place: " + place);

		Assert.assertNotNull("Place not null", place);
		Assert.assertTrue(1 == place.getId());
		Assert.assertTrue("Restaurant Name".equals(place.getName()));
		Assert.assertTrue("Check images", place.getImages().size() > 0);

	}

	@Test
	public void testGetAllPlaces() {
		Place place = new Place();
		place.setName("Restaurant Name");
		place.setDescription("Restaurant description");

		savePlace(place);

		List list = getAllPlaces(1);

		Assert.assertTrue("Check place list >= 0", list.size() >= 0);

	}

	private Place savePlace(Place place) {
		Integer id = new RestTemplate().postForObject(endpoint + "/resource/places", place, Integer.class);
		place.setId(id);

		return place;
	}

	@SuppressWarnings("unchecked")
	private List<Place> getAllPlaces(Integer categoryId) {
		return new RestTemplate().getForObject(endpoint + "/resource/places?categoryId={categoryId}", List.class,
				categoryId);
	}

	private Place getPlace(Integer id) {
		try {
			return new RestTemplate().getForObject(endpoint + "/resource/places/{id}", Place.class, id);
		} catch (HttpServerErrorException e) {
			System.out.println(e.getResponseBodyAsString());
			throw e;
		}
	}

}
