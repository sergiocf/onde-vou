package com.ondevou.integration.test.rest;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;

import com.ondevou.entity.Place;
import com.ondevou.entity.PlaceImage;
import com.ondevou.entity.Review;

public class ITTestReviewRestController {
	
	private String endpoint = "http://localhost:8080/onde-vou";

	@Test
	public void testSaveReview() {
		Place place = new Place();
		place.setName("Restaurant Name");
		place.setDescription("Restaurant description");

		PlaceImage image = new PlaceImage();
		image.setPath("/test/image.jpg");
		place.addImage(image);
		
		savePlace(place);
		
		System.out.println("Finish savePlace");
		
		Review review = new Review();
		review.setAuthor("test@gmail.com");
		review.setDescription("Very good!");
		review.setStars(5);
		place.addReview(review);
		
		System.out.println("Place id review: " + review.getPlace().getId());
		
		review = saveReview(review);
		
		Assert.assertTrue(review.getId() != null);
	}

	private Place savePlace(Place place) {
		Integer id = new RestTemplate().postForObject(endpoint + "/resource/places", place, Integer.class);
		
		System.out.println("Id: " + id);
		
		place.setId(id);

		return place;
	}
	
	private Review saveReview(Review review) {
		System.out.println("Review before:"  + review);
		Integer id = new RestTemplate().postForObject(endpoint + "/resource/reviews", review, Integer.class);
		review.setId(id);

		return review;
	}


}
