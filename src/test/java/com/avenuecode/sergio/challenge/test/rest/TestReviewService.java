package com.avenuecode.sergio.challenge.test.rest;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.ondevou.entity.Place;
import com.ondevou.entity.Review;
import com.ondevou.repository.ReviewRepository;

public class TestReviewService {
	
	@Mock
	private ReviewRepository repository;
	
	
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}
	
    @Test
	public void testRateAverage() {
		Review newReview = null;
		
        when(repository.saveReview(newReview)).thenReturn(getFakeReview()); 
        newReview = repository.saveReview(newReview);
        
        Place place = newReview.getPlace();
        
        assertThat("Calculate rate 3.5", place.calculateRate().compareTo(3.5f), is(0));
        
        newReview = new Review();
        newReview.setStars(5);
        place.addReview(newReview);
        
        assertThat("Calculate rate 4", place.calculateRate().compareTo(4f), is(0));
	}
	
    public Review getFakeReview() {
    	Place place = new Place();
    	
    	Review review = new Review();
    	review.setStars(3);
    	place.addReview(review);
    	
    	review = new Review();
    	review.setStars(4);
    	place.addReview(review);
      	
    	return review;   	    	
    }

}
