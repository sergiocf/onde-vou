package com.ondevou.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ondevou.entity.Place;
import com.ondevou.entity.Review;
import com.ondevou.repository.ReviewRepository;

@Service
public class ReviewService {
	
	@Autowired
	ReviewRepository repository; 
	
	@Autowired
	PlaceService placeService;
	
	@Transactional
	public Integer saveReview(Review review) {
		System.out.println("New Review: " + review);
		review = repository.saveReview(review);		
		
		return review.getId();
		
	}
	
	@Transactional
	public void calculateRate(Integer id) {
		Place place =  placeService.getPlace(id);
		
		System.out.println("Place review: " + place);
		place.setRating(place.calculateRate());
		
		placeService.savePlace(place);

		
	}

	
	

}
