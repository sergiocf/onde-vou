package com.ondevou.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ondevou.entity.Place;
import com.ondevou.repository.PlaceRepository;

@Service
public class PlaceService {		
	
	@Autowired
	private PlaceRepository repository;

	@Transactional
	public Place getPlace(Integer id) {		
		return repository.getPlace(id);
	}
	
	@Transactional
    public Integer savePlace(Place place) {
    	return repository.savePlace(place);
    }

	@Transactional
	public List<Place> getAllPlacesByCategory(Integer categoryId) {
		return repository.getAllPlacesByCategory(categoryId);
	}	

}
