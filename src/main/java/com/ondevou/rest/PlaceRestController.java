package com.ondevou.rest;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ondevou.entity.Place;
import com.ondevou.service.PlaceService;
import com.ondevou.util.DistanceCalculator;

@RestController
@RequestMapping("/places")
public class PlaceRestController {

	@Autowired
	private PlaceService placeService;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = { "application/json" })
	public Place getPlace(@PathVariable Integer id) {
		Place place = placeService.getPlace(id);
		return place;
	}

	@RequestMapping(method = RequestMethod.POST, consumes = { "application/json" }, produces = { "application/json" })
	public Integer savePlace(@RequestBody Place place) {
		Integer id = placeService.savePlace(place);

		return id;
	}

	@RequestMapping(params = { "categoryId" }, method = RequestMethod.GET, produces = { "application/json" })
	public List<Place> getAllPlacesByCategory(@RequestParam Integer categoryId, @RequestParam Double latitude,
			@RequestParam Double longitude) {

		List<Place> places = placeService.getAllPlacesByCategory(categoryId);

		for (Place place : places) {
			place.setImages(null);
			place.setReviews(null);

			place.setDistance(DistanceCalculator.distance(latitude, longitude, place.getLocation().getCoordinate().x,
					place.getLocation().getCoordinate().y, "K"));
			NumberFormat formatter = new DecimalFormat("#0.00");
			System.out.println(formatter.format(place.getDistance()));

		}

		Collections.sort(places, new Comparator<Place>() {

			@Override
			public int compare(Place o1, Place o2) {
				return o1.getDistance().compareTo(o2.getDistance());
			}

		});

		return places;
	}

}
