package com.ondevou.rest;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ondevou.entity.Review;
import com.ondevou.service.ReviewService;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;
import com.vividsolutions.jts.operation.distance.DistanceOp;

@RestController
@RequestMapping("/reviews")
public class ReviewRestController {
	
	@Autowired
	ReviewService service;
	
	@RequestMapping(method = RequestMethod.POST, consumes = { "application/json" }, produces = { "application/json" })
	public Integer saveReview(@RequestBody Review review) {		
		Integer id = service.saveReview(review);
		
		service.calculateRate(review.getPlace().getId());
		
		
		
		return id;
	}
	
	public static void main(String[] args) throws ParseException {
		WKTReader reader = new WKTReader();
		
		Geometry g0 = reader.read(String.format("POINT(%s %s)",-19.955623, -43.934519));
		Geometry g1 = reader.read(String.format("POINT(%s %s)",-19.957155, -43.938146));
		 
		double distance = DistanceOp.distance(g0, g1);
		
		NumberFormat formatter = new DecimalFormat("#0.00");    
		System.out.println("Dist: " + formatter.format(distance));
	}

}
