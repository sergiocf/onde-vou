package com.ondevou.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
public class PlaceImage implements Serializable {

	private static final long serialVersionUID = 3770013191304370992L;
	
	@Id
	@GeneratedValue
	private Integer id;
	
	private String path;
	
	@Type(type="text")
	private String content;
	
	@JsonBackReference
	@ManyToOne
	private Place place;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Place getPlace() {
		return place;
	}

	public void setPlace(Place place) {
		this.place = place;
	}

	@Override
	public String toString() {
		return "PlaceImage [id=" + id + ", path=" + path + ", place=" + place.getName() + "]";
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
}
