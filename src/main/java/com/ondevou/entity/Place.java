package com.ondevou.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ondevou.util.JsonToPointDeserializer;
import com.ondevou.util.PointToJsonSerializer;
import com.vividsolutions.jts.geom.Point;


@Entity
public class Place implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Integer id;
	
	private String name;
	
	private String description;
	
	private Float rating;
	
	@JsonBackReference
	@ManyToOne
	private Category category;
	
	@OneToMany(mappedBy="place", cascade={CascadeType.ALL}) 
	@JsonManagedReference
	private List<PlaceImage> images;
	
	@Type(type="text")
	private String logo;
	
	@Transient
	private Double distance;
	
	@OneToMany(mappedBy="place", cascade={CascadeType.ALL}) 
	@JsonManagedReference
	private Set<Review> reviews;
	
	@JsonSerialize(using = PointToJsonSerializer.class)
	@JsonDeserialize(using = JsonToPointDeserializer.class)
	@Column(columnDefinition="Geometry")
	@Type(type="org.hibernate.spatial.GeometryType")
	private Point location;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Float getRating() {
		return rating;
	}

	public void setRating(Float rating) {
		this.rating = rating;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}
	
	public List<PlaceImage> getImages() {
		return images;
	}

	public void setImages(List<PlaceImage> images) {
		this.images = images;
	}
	
	public void addImage(PlaceImage placeImage) {
		if(images == null) {
			images = new ArrayList<>();
		}
		
		placeImage.setPlace(this);
		images.add(placeImage);
	}
	
	public void addReview(Review review) {
		if(reviews == null) {
			reviews = new LinkedHashSet<>();
		}
		
		review.setPlace(this);
		reviews.add(review);
		
	}

	public Double getDistance() {
		return distance;
	}

	public void setDistance(Double distance) {
		this.distance = distance;
	}	
	
	@JsonProperty
	public String getSnippet() {
		if( description == null ){
			return null;			
		}
		
		final int SNIPPET_SIZE = 70;
		
		if(description.length() > SNIPPET_SIZE) {
			return description.substring(0,  SNIPPET_SIZE) + "...";
		}
		
		return description; 				
	}
	
	@JsonIgnore
	public void setSnippet(String snippet) {
		
	}
	
	public Set<Review> getReviews() {
		return reviews;
	}

	public void setReviews(Set<Review> reviews) {
		this.reviews = reviews;
	}
	
	public Point getLocation() {
		return location;
	}

	public void setLocation(Point location) {
		this.location = location;
	}
	
	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public Float calculateRate() {
		Float sum = 0f;
		
		for(Review review: reviews) {
			sum += review.getStars();
		}
		
		return sum / reviews.size();
	}
	
	@Override
	public String toString() {
		return "Place [id=" + id + ", name=" + name + ", description=" + description + ", rating=" + rating
				+ ", category=" + category + ", images=" + images + ", distance=" + distance
				+ ", reviews=" + reviews + "]";
	}

	
}
