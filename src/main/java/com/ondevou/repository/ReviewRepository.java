package com.ondevou.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.ondevou.entity.Review;

@Repository
public class ReviewRepository {
	
	@PersistenceContext
    EntityManager em;
	
	public Review saveReview(Review review) {
		em.persist(review);
		
		return review;
	}

}
