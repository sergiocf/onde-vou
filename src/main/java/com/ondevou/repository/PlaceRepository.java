package com.ondevou.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;

import com.ondevou.entity.Place;

@Repository
public class PlaceRepository {

	@PersistenceContext
    EntityManager em;

	public Place getPlace(Integer id) {
		Query query = em.createQuery("from Place p join fetch p.images where p.id = :id ");
		query.setParameter("id", id);
		Place place = (Place)query.getSingleResult();
		
		Hibernate.initialize(place.getReviews());
		
		return place;
	}	
	
	public Integer savePlace(Place place) {
		em.persist(place);
		
		return place.getId();
	}

	@SuppressWarnings("unchecked")
	public List<Place> getAllPlacesByCategory(Integer categoryId) {
		Query query = em.createQuery("from Place p where p.category.id = :categoryId ");
		query.setParameter("categoryId", categoryId);
		
		return query.getResultList();
	}

}
