package com.ondevou.util;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.vividsolutions.jts.geom.Point;

public class PointToJsonSerializer extends JsonSerializer<Point> {

	@Override
	public void serialize(Point value, JsonGenerator jgen, SerializerProvider provider)
			throws IOException, JsonProcessingException {
		try {
			if (value != null) {
				double lat = value.getX();
				double lon = value.getY();

				jgen.writeStartObject();
				jgen.writeNumberField("lat", lat);
				jgen.writeNumberField("lng", lon);
				jgen.writeEndObject();

			}
		} catch (Exception e) {
		}

	}

}
