'use strict';

/* Services */
var comovouServices = angular.module('comovouServices', ['ngResource']);

comovouServices.factory('Place', ['$resource',
  function($resource){
    return $resource('resource/places/:id', {}, {
      query: {method:'GET', params:{}, isArray:true}
    });
  }]);

comovouServices.factory('Review', ['$resource',
  function($resource){
    return $resource('resource/reviews/:id', {}, {
      save:  {method:'POST'},
      query: {method:'GET', params:{}, isArray:true}
    });
  }]);


