'use strict';

/* App Module */

var comovouApp = angular.module('comovouApp', [
  'ngRoute',
  'placeAnimations',
  'comovouControllers',
  'phonecatFilters',
  'comovouServices'
]);

comovouApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/menu', {
        templateUrl: 'app/partials/menu-list.html',
        controller: 'MenuCtrl'
      }).
      when('/places/list=:categoryId', {
        templateUrl: 'app/partials/place-list.html',
        controller: 'PlaceListCtrl'
      }).
      when('/places/:placeId', {
          templateUrl: 'app/partials/place-detail.html',
          controller: 'PlaceDetailCtrl'
      }).
      when('/place-add', {
          templateUrl: 'app/partials/place-add.html',
          controller: 'PlaceAddCtrl'
      }).
      otherwise({
        redirectTo: '/menu'
      });
  }]);
