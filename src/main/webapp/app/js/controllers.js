'use strict';



var newLocation;

var map;

var directionsService = null;
	
var directionsDisplay = null;

var marker;

/* Controllers */
var doSomething = (function () {
	  "use strict";
	   return {
		   initMap: (function (myLatLng) {

			   map = new google.maps.Map(document.getElementById('map'), {
						center : myLatLng,
							zoom : 17,
							disableDefaultUI: true
					});
			   
			   var types = document.getElementById('type-selector');
			   
			   map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);



				marker = new google.maps.Marker({
					position : myLatLng,
					map : map,
					title : "Lugar"
				});
	    	  
	    	  
	        return console.log('test 2');
	      }),
	      
	      initMapAdd: (function (myLatLng) {
	    	  
	    	  var map = new google.maps.Map(document.getElementById('myMap'), {
	    		    center: myLatLng,
	    		    zoom: 13,
	    		    disableDefaultUI: true
	    		  });
	    	  
    		  var input = /** @type {!HTMLInputElement} */(
    		      document.getElementById('pac-input'));

    		  //var types = document.getElementById('type-selector');
    		  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    		  //map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

    		  var autocomplete = new google.maps.places.Autocomplete(input);
    		  autocomplete.bindTo('bounds', map);

    		  var infowindow = new google.maps.InfoWindow();
    		  var marker = new google.maps.Marker({
    		    map: map,
    		    anchorPoint: new google.maps.Point(0, -29)
    		  });

    		  autocomplete.addListener('place_changed', function() {
    		    infowindow.close();
    		    marker.setVisible(false);
    		    var place = autocomplete.getPlace();
    		    if (!place.geometry) {
    		      window.alert("Autocomplete's returned place contains no geometry");
    		      return;
    		    }
    		    
    		    newLocation = place.geometry.location;
    		    
    		    // If the place has a geometry, then present it on a map.
    		    if (place.geometry.viewport) {
    		      map.fitBounds(place.geometry.viewport);
    		    } else {
    		      map.setCenter(place.geometry.location);
    		      map.setZoom(17);  // Why 17? Because it looks good.
    		    }
    		    marker.setIcon(/** @type {google.maps.Icon} */({
    		      url: place.icon,
    		      size: new google.maps.Size(71, 71),
    		      origin: new google.maps.Point(0, 0),
    		      anchor: new google.maps.Point(17, 34),
    		      scaledSize: new google.maps.Size(35, 35)
    		    }));
    		    marker.setPosition(place.geometry.location);
    		    marker.setVisible(true);

    		    var address = '';
    		    if (place.address_components) {
    		      address = [
    		        (place.address_components[0] && place.address_components[0].short_name || ''),
    		        (place.address_components[1] && place.address_components[1].short_name || ''),
    		        (place.address_components[2] && place.address_components[2].short_name || '')
    		      ].join(' ');
    		    }

    		    infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
    		    infowindow.open(map, marker);
    		  });
	    	  
	    	  
	    	  
	    	  
	    	  
				
	      }),
	      
	      initMapDirection: (function (origin, destination, mode) {
	    	  
	    	  var travelMode;
	    	  
	    	  if(mode === 'driving') {
	    		  travelMode = google.maps.TravelMode.DRIVING;
	    		  
	    	  } else if (mode === 'bus') {
	    		  travelMode = google.maps.TravelMode.TRANSIT;
	    		  
	    	  } else {
	    		  travelMode = google.maps.TravelMode.WALKING;
	    		  
	    	  }
	    	  
	    	  directionsDisplay.setMap(map);
	    	  
	    	  
			  directionsService.route({origin: origin, destination: destination, travelMode: travelMode}, function(response, status) {
			    if (status === google.maps.DirectionsStatus.OK) {
			      directionsDisplay.setDirections(response);
			    } else {
			      window.alert('Directions request failed due to ' + status);
			    }
			  });
		    	
	      })
	   };
	}());



var comovouControllers = angular.module('comovouControllers', []);

comovouControllers.controller('MenuCtrl', ['$scope', 'Place', '$location', '$rootScope',
  function($scope, Place, $location, $rootScope) {
	
	
	$rootScope.category = { RESTAURANT: {value: 1, name: "Restaurante" }, 
			THEATHER: {value: 2, name: "Cinema" }, 
			NIGHTCLUB: {value: 3, name: "Boate" }, 
			PARK: {value: 4, name: "Parque" }
	};
	
	var gpsSunccuss = function(position) {
	    $rootScope.currentLatitude = position.coords.latitude; 
	    $rootScope.currentLongitude = position.coords.longitude;
	    
	};
	
	navigator.geolocation.getCurrentPosition(gpsSunccuss);
	
	
    $scope.selectCategory = function(category) {  
    	
    	$location.path('places/list=' + category);
    	
    	//+ '&latitude=' + $rootScope.currentLatitude + '&longitude=' + $rootScope.currentLongitude
    }    
    
  }]);

comovouControllers.controller('PlaceListCtrl', ['$scope', '$routeParams', 'Place', '$log','$rootScope', 
  function($scope, $routeParams, Place, $log, $rootScope) {
	
    var category = $scope.category[$routeParams.categoryId];    
    $scope.title = category.name;
    
	var gpsSunccuss = function(position) {
	    $rootScope.currentLatitude = position.coords.latitude; 
	    $rootScope.currentLongitude = position.coords.longitude;
	    
	    $scope.places = Place.query({categoryId: category.value, latitude: $rootScope.currentLatitude, longitude: $rootScope.currentLongitude});
	    
	};
	
	navigator.geolocation.getCurrentPosition(gpsSunccuss);
	
    
    
  }]);

comovouControllers.controller('PlaceDetailCtrl', ['$scope', '$routeParams', 'Place', 'Review', '$log', '$rootScope',                                                     
  function($scope, $routeParams, Place, Review, $log, $rootScope) {
	
	$scope.tab = 1;
	$scope.review = {};
	$scope.travelMode = 'position';
	directionsService = new google.maps.DirectionsService;
	directionsDisplay = new google.maps.DirectionsRenderer;
	 
	
	$scope.place = Place.get({id: $routeParams.placeId}, function(place) {
	      $scope.mainImageUrl = place.logo;
	      
	      doSomething.initMap(place.location);
	      
	});
	
		
	$scope.changeMap = function(){
		var origin = {};
		origin.lat = $rootScope.currentLatitude;
		origin.lng = $rootScope.currentLongitude;
		
		if($scope.travelMode === 'position') {
			
		      //var position = new google.maps.LatLng(-12.461334, 130.841904);
		      map.setCenter($scope.place.location);
		      map.setZoom(17);
  		      marker.setVisible(true);
	    	  directionsDisplay.setMap(null);


			
			//doSomething.initMap();
			
		} else {
			marker.setVisible(false);
			doSomething.initMapDirection(origin, $scope.place.location, $scope.travelMode);
		}
		
    };	
	
	
	$scope.setTab = function(tab){
		$scope.tab = tab;
    };

    $scope.isSet = function(tab){
        return ($scope.tab === tab);
    };

    $scope.setImage = function(imageUrl) {
    	$scope.mainImageUrl = imageUrl;
    };
    
    $scope.addReview = function(place) {
    	var newReview = new Review();
    	newReview.stars = $scope.review.stars;
    	newReview.description = $scope.review.description;
    	newReview.author = $scope.review.author;
    	
    	$log.log('Saving review - place : ' + place.id);
    	
    	newReview.place = {};
    	
    	newReview.place.id = place.id;
    	
    	newReview.$save();
    	
    	place.reviews.push($scope.review);
	    $scope.review = {};
    };
    
  }]);

comovouControllers.controller('PlaceAddCtrl', ['$scope', '$routeParams', 'Place', '$log','$rootScope', 
  function($scope, $routeParams, Place, $log, $rootScope) {
	$scope.place = {};
	$scope.place.category = {};
	$scope.place.images = [];
	
	var myLocation = {};
	
	if($rootScope.currentLatitude && $rootScope.currentLongitude) {
		myLocation.lat = $rootScope.currentLatitude;
		myLocation.lng = $rootScope.currentLongitude;
	} else {
		myLocation = {lat: -33.8688, lng: 151.2195};
		
	}
	
    doSomething.initMapAdd(myLocation);
	
	var file = $('.fileinput').fileinput();
	
	file.on('change.bs.fileinput', function(e, files) {	
		var reader  = new FileReader();
		
		var placeImage= {};
		
		var isLogo = this.id === 'logo'? true : false;	 
		  
		reader.onloadend = function () {
			
			if(isLogo) {
				$scope.place.logo = reader.result;
				
			} 

			placeImage.content = reader.result;
			placeImage.path = files.name; 
			$scope.place.images.push(placeImage);
				
		}
		
		if (files) {
			reader.readAsDataURL(files);
		} 
		
		
		
	});

	
	$scope.addPlace = function(place) {
    	
		var newPlace = new Place();
    	newPlace.name = place.name;
    	newPlace.description = place.description;
    	newPlace.category = {};
    	newPlace.category.id = place.category.id;
    	newPlace.images = $scope.place.images;	
    	newPlace.logo = $scope.place.logo;
    	
    	if(!newLocation){
    		alert('Informe localizacao');
    		return;    		
    	} 
    	
    	newPlace.location = '';
    	newPlace.location = newLocation.lat() + ':' + newLocation.lng();
    	    	
    	newPlace.$save(function() {
    		alert('Salvo!');
    	    doSomething.initMapAdd(myLocation);
    	    $scope.place = {};
        	$scope.place.category = {};
        	$scope.place.images = [];
        	newLocation = null;
        	
    
    	});
    	
    	var fileinput = $('.fileinput').fileinput('clear');

    		    	
    		
    };
                                              	 
  
}]);

